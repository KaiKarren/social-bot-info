## intent:startInfo
- start info
- start new info
- hi
- hey
- hello
- Einführung
- Gib mir eine Einführung
- Wer bist du?
- Was bist du?
- Wie kannst du mir helfen?
- Was willst du?

##intent:help
- hilfe
- help
- kannst du mir bei deiner Verwendung helfen?
- Kannst du mir helfen?
- Wie benutzt man dich?

## intent:options
- Was kannst du alles?
- Was kannst du mir erzählen?
- infos?
- Sag mir was du alles kanst.
- Über was kannst du mich informieren?
- Wozu kannst du mir Informationen geben?

## intent:whatisASocialBot
- Was ist ein Social Bot?
- Was versteht man unter einem Social Bot?
- Social Bot?
- Was macht einen Social Bot aus?
- Was unterscheidet einen social Bot von einem Bot?
- Was unterscheidet einen social Bot von einem normalen Bot?
- Was bedeutet social Bot?
- Was bedeutet social Bot eigentlich?

## intent:creator
- Wer hat dich erschaffen?
- Warum würdest du erschaffen?
- Was ist dein Zweck?
- Warum gibt es dich?
- Was ist dein Ziel?
- Wer ist dein Schöpfer?
- Wer hat dich konstruiert?

## intent:sources
- Woher hast du deine Infos?
- Woher hast du deine Informationen?
- Worauf stützt du deine Aussagen?
- Was sind deine Quellen?
- Quellen?
- Welche Paper wurden benutzt?
- Paper?
- Benutzte Paper
- paper?

## intent:usage
- Wozu werden Bots verwendet?
- Wozu verwendet man social Bots?
- Verwendung?
- Was kann ein Social Bot anrichten?
- Was kann ein social Bot alles anrichten?
- Für was kann man Social Bots einsetzen?
- Was sind die Einsatzmöglichkeiten von social Bots?
- Wozu können social Bots verwendet werden?
- Für was setzt man social Bots ein?
- Wofür werden social Bots eingesetzt?
- Wo findet man social Bots?
- Wo kann man social Bots finden?
- Wozu werden social Bots eingesetzt?
- Wo werden social Bots eingesetzt?

## intent:howToDetect
- Wie kann man Bots erkennen?
- Wie kann man social Bots erkennen?
- Wie kann man sie erkennen?
- Kann man sie erkennen?
- Wie kann man sie erkennen?
- Welche Möglichkeiten hat man um sie zu erkennen?
- Gibt es einen Weg social Bots zu erkennen?
- Welche Möglichkeiten gibt es um social Bots zu erkennen?

## intent:whatIsCrowdSourcing
- Was ist Crowdsourcing?
- Was versteht man unter Crowdsourcing?
- crowdsourcing?
- Crowdsourcing?
- Was bedeutet Crowdsourcing?

## intent:graphBasedDetection
- Wie funktioniert die netzwerkbasierte Erkennung?
- Wie funktioniert die Erkennung mit sozialen Netzwerkinformationen?
- Erkennung mit Netzwerkinformationen?
- Wie funktioniert die Erkennung mit Hilfe von Netzwerkinformationen?
- Wie funktioniert das mit Netzwerkinformationen?
- Und wie funktioniert die netzwerkbasierte Erkennung?

## intent:crowdsourcedDetection
- Wie funktioniert Crowdsourcing bei der Erkennung?
- Wie geht die Erkennung mit Crowdsourcing?
- Erkennung mit Crowdsourcing?
- Wie funktioniert die Erkennung per Crowdsourcing
- Wie funktioniert der Crowdsourcing Ansatz?

## intent:featureBasedDetect
- Wie funktioniert die merkmalsbasierte Erkennung?
- merkmalsbasierte Erkennung?
- Wie geht die merkmalsbasierte Erkennung?
- Wie funktioniert das merkmalsbasiert?
- merkmalsbasierte Erkennung?

## intent:howWorkAllDetections
- Wie funktionieren diese Methoden genau?
- Wie funktionieren die Erkennungsmethoden?
- Wie funktionieren alle Erkennungsmethoden?
- Kannst du mir erklären wie alle Erkennungsmethoden funktionieren?
- Kannst du mir sagen wie diese Erkennungsmethoden funktionieren?

## intent:canTheyManipulateUs
- Können social Bots uns beeinflussen?
- Können social Bots uns wirklich beeinflussen?
- Können sie uns beeinflussen?
- Können wir manipuliert werden?
- Sind social Bots wirklich in der Lage uns zu manipulieren?

## intent:overallResultsAndPrevention
- Was für Erkenntnisse sollten wir daraus ziehen?
- Und was sollen wir jetzt tuen?
- Und was heißt das jetzt?
- Was kann man dagegen tuen?
- Was kann man machen um sich einer möglichen Beeinflussung zu entziehen?
- Wie kann man sich schützen?
- Wie kann man sich vor ihnen schützen?
- Was kann man tun um sich zu schützen?

## intent:whatAreTheGoalsOfSocialBots
- Was sind die Ziele von Social Bots?
- Welche Ziele haben Bots?
- Warum machen social Bots das?
- Welche Ziele verfolgen social Bots?
- Was sind ihre Ziele?
- Welche Ziele haben social Bots?

## intent:yes
- Ja
- Ja bitte
- Das wäre gut.
- Das fände ich gut.

## intent:thanks
- Danke
- Dankeschön
- Danke dir
- Vielen Dank
- danke für deine Hilfe
- danke für die Informationen

## intent:howisSusceptible
- Wer ist anfällig für social Bots?
- Welche Merkmale machen jemanden anfällig für social Bots?
- Wann ist man anfällig?
- Wer ist anfällig?
- Welche Merkmale machen anfällig für social Bots?
- Wer ist anfälliger?
- Was macht mich anfällig für social Bots?