## basicFunctions1
* startInfo
    - utter_startInfo
* options
    - utter_options
* whatisASocialBot
    - utter_whatisASocialBot
* sources
    - utter_sources
* creator
    - utter_creator

## introduction
* startInfo
    - utter_startInfo
* creator
    - utter_creator

## introduction2
* startInfo
    - utter_startInfo
* whatis
    - utter_whatisASocialBot

## help
* help
    - utter_help

## sourcesStory
* sources
    - utter_sources

## optionsStory
* options
    - utter_options

## creatorStory
* creator
    - utter_creator

## startInfo
* startInfo
    - utter_startInfo

## whatIsASocialBot
* whatisASocialBot
    - utter_whatisASocialBot

## usageStory
* usage
    - utter_einsatzmoeglichkeitenIntro
    - utter_einsatz1
    - utter_einsatz2
    - utter_einsatz3
    - utter_einsatz4
    - utter_einsatz5
    - utter_einsatz6
    - utter_einsatz7

## detectionStory
* howToDetect
    - utter_botdetectionIntro
    - utter_botdetect1
    - utter_botdetect2
    - utter_botdetect3
    - utter_botdetect4

## crowdsourcing
* whatIsCrowdSourcing
    - utter_whatIsCrowdSourcing

## detectGraphBased
* graphBasedDetection
    - utter_graphBasedDetection
    - utter_graphBasedDetectionProb

## detectCrowdsource
* crowdsourcedDetection
    - utter_crowdSourcedDetec
    - utter_crowdSourcedDetecProb

## detectFeatureBased
* featureBasedDetect
    - utter_featureBasedDetect

## allDetections
* howWorkAllDetections
    - utter_graphBasedDetection
    - utter_graphBasedDetectionProb
    - utter_crowdSourcedDetec
    - utter_crowdSourcedDetecProb
    - utter_featureBasedDetect

## manipulationStory
* canTheyManipulateUs
    - utter_canTheyManipulateUs
    - utter_manipulationImplications
    - utter_manipulationImplEnd

## manipulationStory2
* canTheyManipulateUs
    - utter_canTheyManipulateUs
    - utter_manipulationImplications
    - utter_manipulationImplEnd
* yes
    - utter_impactAndInfluenceExperimentIntro
    - utter_impactAndInfluenceHasHeImpactNow
    - utter_impactAndInfluenceReactions
    - utter_impactAndInfluenceAuthorInterpretation
    - utter_impactAndInfluenceReaktion2
    - utter_impactAndInfluenceBemerkung

## preventionStory
* overallResultsAndPrevention
    - utter_overallResultsAndPrevention

## goalStory
* whatAreTheGoalsOfSocialBots
    - utter_whatAreTheGoalsOfSocialBots

## thankYouStory
* thanks
    - utter_thanks

## susceptibleStory
* howisSusceptible
    - utter_suspShortSum
    - utter_suspMain
    - utter_suspFeatures
    - utter_suspNote
