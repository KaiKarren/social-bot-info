"""
Creates a connection to Telegram through the Python Telegram Bot API (https://python-telegram-bot.readthedocs.io/en/stable/) which needs to be installed like this (https://github.com/python-telegram-bot/python-telegram-bot#installing).

Created for the Medienproject "Story Bot" at Saarland University
by Marc Ruble in cooperation with Kai Karren

If you want to use this script please contact our GitLab group StoryBot to ask for the permission
Without the permission the use is forbidden.

written by Kai Karren on 26th Dez 2018

"""

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

import time

import rasa_core
from rasa_core.agent import Agent
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.policies.memoization import MemoizationPolicy
from rasa_core.interpreter import RasaNLUInterpreter
from rasa_core.utils import EndpointConfig
from rasa_core import config

# define global agent
agent = ""

# waits for seconds
def delay(seconds):
	time.sleep(seconds)

# starts the rasa bot accessible by global agent
def start_rasa_bot():
	interpreter = RasaNLUInterpreter('./models/nlu/default/chat')
	action_endpoint = EndpointConfig(url="http://localhost:5055/webhook")
	global agent
	agent = Agent.load('./models/dialogue', interpreter=interpreter, action_endpoint=action_endpoint)

# react to a users message
def rasa_answer(bot, update):
	# get users message
	message = update.message
	print(str(message.from_user.id) + " says: " + message.text)
	
	# get bots answers
	global agent
	answers = agent.handle_text(message.text, sender_id=str(message.from_user.id))
	
	# send all of bots answers
	for answer in answers:
		print("Bot says: " + answer['text'])
		bot.send_chat_action(update.message.chat.id, "typing")
		delay(1)
		update.message.reply_text(answer['text'])

def main():
	# start the rasa bot instance
	start_rasa_bot()
	
	# configure telegram bot connection
	updater = Updater() # insert token=... in the constructor
	dispatcher = updater.dispatcher
	
	print("--- BOT STARTED ---")
	
	# let rasa handle the messages
	rasa_answer_handler = MessageHandler(Filters.text, rasa_answer)
	dispatcher.add_handler(rasa_answer_handler)
	
	# start listening to telegram bot
	updater.start_polling()
	
	# wait till ended
	updater.idle()
	
	print("--- BOT ENDED ---")
	
if __name__ == '__main__':
	main()